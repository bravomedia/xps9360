#!/bin/bash

sudo apt-get install git libsmbios-dev libefivar-dev cmake libdbus-c++-dev libnl-3-dev libnl-genl-3-dev libdbus-1-dev

dir="$(pwd)"
 
repos=(
    https://github.com/dell/thunderbolt-icm-dkms.git
    https://github.com/dell/thunderbolt-nvm-linux.git
    https://github.com/01org/thunderbolt-software-user-space.git
)

for repo in ${repos[@]}; do
    name="$(basename "$repo")"
    name="${name%%.*}"
    if [[ ! -d "$name" ]]; then
        git clone --recursive "$repo"
    else
        pushd "$name" || exit 1
            git pull
        popd
    fi
done

pushd thunderbolt-icm-dkms || exit 1
    sudo rm -rf /usr/src/thunderbolt-icm-1.0
    sudo cp -r thunderbolt-icm /usr/src/thunderbolt-icm-1.0 || exit 1
    sudo dkms build -m thunderbolt-icm -v 1.0 || exit 1
    sudo dkms install -m thunderbolt-icm -v 1.0 || exit 1
popd

sudo modprobe thunderbolt-icm || {
    echo "thunderbolt-icm module is not loaded, restart?"
    exit 1
}

pushd thunderbolt-nvm-linux || exit 1
    gcc -o "$dir/force_dell_tbt" force_dell_tbt.c -I /usr/include/efivar/ -lsmbios_c -lefivar || exit 1
popd

pushd thunderbolt-software-user-space || exit 1
    git checkout fwupdate || exit 1

    mkdir thunderbolt-daemon-build
    pushd thunderbolt-daemon-build || exit 1
        cmake ../ThunderboltService/Linux -DCMAKE_BUILD_TYPE=Release || exit 1
        make || exit 1
        sudo make install || exit 1
    popd

    mkdir libtbtfwu-build
    pushd libtbtfwu-build || exit 1
        cmake ../fwupdate/libtbtfwu -DCMAKE_BUILD_TYPE=Release || exit 1
        make || exit 1
        sudo make install || exit 1
    popd

    mkdir tbtfwucli-build
    pushd tbtfwucli-build || exit 1
        cmake ../fwupdate/tbtfwucli -DCMAKE_BUILD_TYPE=Release || exit 1
        make || exit 1
        sudo make install || exit 1
    popd
popd


echo "
---------------------------------------------------------------------------

    # To force flashing mode:
    sudo ./force_dell_tbt 1

    # To force user mode:
    sudo ./force_dell_tbt 0

    # Flash
    tbtfwucli EnumControllers
    tbtfwucli ValidateFWImage \$CONTROLLERID \$PAYLOAD
    tbtfwucli FWUpdate \$CONTROLLERID \$PAYLOAD

---------------------------------------------------------------------------
"
